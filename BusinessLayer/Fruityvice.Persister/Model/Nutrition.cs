﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fruityvice.Persister.Model
{
    public class Nutrition
    {
        public Nutrition NewNutrition()
        {

            return new Nutrition();
        }
        public int id { get; set; }
        public int fruitsid { get; set; }
        public int carbohydrates { get; set; }
        public int protein { get; set; }
        public int fat { get; set; }
        public int calories { get; set; }
        public int sugar { get; set; }
    }
}
