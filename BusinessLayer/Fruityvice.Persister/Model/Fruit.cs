﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fruityvice.Persister.Model
{
    public class Fruit
    {
        public Fruit NewFruit()
        {

            return new Fruit();
        }
        public string? name { get; set; }
        public int id { get; set; }
        public string? family { get; set; }
        public string? genus { get; set; }
        public string? order { get; set; }
    }
}
