﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fruityvice.Persister.Model
{
    public class NutritionInfo
    {
        public NutritionInfo NewNutritionInfo()
        {

            return new NutritionInfo();
        }
       
        public decimal carbohydrates { get; set; }
        public decimal protein { get; set; }
        public decimal fat { get; set; }
        public decimal calories { get; set; }
        public decimal sugar { get; set; }
    }
}
