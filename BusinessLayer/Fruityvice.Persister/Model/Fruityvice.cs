﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fruityvice.Persister.Model
{
    public class Fruityvices
    {
        public Fruityvices NewFruityvice()
        {

            return new Fruityvices();
        }


        public string? name { get; set; }
        public int id { get; set; }
        public string? family { get; set; }
        public string? genus { get; set; }
        public string? order { get; set; }
        public NutritionInfo nutritionInfo { get; set; }

    }
}
