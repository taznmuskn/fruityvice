﻿using Fruityvice.Dal;
using Fruityvice.Persister.Persister;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fruityvice.Persister.Model
{
    public static class ModelConfigurationExtension
    {
        public static void AddModel(this IServiceCollection services)
        {
            services.AddTransient<Fruit, Fruit>();
            services.AddTransient<Fruityvices, Fruityvices>();
            services.AddTransient<Nutrition, Nutrition>();
            services.AddTransient<FruitInfo, FruitInfo>();
            services.AddTransient<NutritionInfo, NutritionInfo>();
        }
    }
}
