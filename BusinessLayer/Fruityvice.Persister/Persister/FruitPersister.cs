﻿using DBContext.Entity;
using Fruityvice.Dal;
using Fruityvice.Persister.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Fruityvice.Persister.Persister
{
    public class FruitPersister
    {
        private readonly IFruitsDal fruitDal;
        public FruitPersister(IFruitsDal fruitDal)
        {
            this.fruitDal = fruitDal;
        }

        public Fruit Insert(Fruit fruit)
        {

            Fruits fruits = new Fruits();
            fruits.order = fruit.order;
            fruits.genus = fruit.genus;
            fruits.name = fruit.name;
            fruits.family = fruit.family;
            fruitDal.Insert(fruits);
            fruit.id = fruits.id;
            return fruit;
        }
        public Fruit Update(Fruit fruit)
        {
            Fruits fruits = new Fruits();
            fruits.order = fruit.order;
            fruits.genus = fruit.genus;
            fruits.name = fruit.name;
            fruits.family = fruit.family;
            fruitDal.Update(fruits);
            fruit.id = fruits.id;
            return fruit;
        }
        public Fruit Delete(Fruit fruit)
        {
            Fruits fruits = new Fruits();
            fruits.order = fruit.order;
            fruits.genus = fruit.genus;
            fruits.name = fruit.name;
            fruits.family = fruit.family;
            fruitDal.Delete(fruits);
            fruit.id = fruits.id;
            return fruit;
        }

        public List<Fruit> GetFruits()
        {
            List<Fruit> list = new List<Fruit>();
            foreach (var item in fruitDal.FetchList())
            {
                Fruit fruit = new Fruit();
                fruit.order = item.order;
                fruit.family = item.family;
                fruit.genus = item.genus;
                fruit.name= item.name;
                fruit.id= item.id;
                list.Add(fruit);
            }
            return list;
        }
    }
}
