﻿using Fruityvice.Persister.Model;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fruityvice.Persister.Persister
{
    public static class PersisterConfigurationExtension
    {
        public static void AddPersister(this IServiceCollection services)
        {
            services.AddTransient<FruitPersister, FruitPersister>();
            services.AddTransient<NutritionPersister, NutritionPersister>();
            services.AddTransient<FruityvicePersiter, FruityvicePersiter>();
        }
    }
}
