﻿using DBContext.Entity;
using Fruityvice.Dal;
using Fruityvice.Persister.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fruityvice.Persister.Persister
{
    public class NutritionPersister
    {
        private readonly INutritionsDal nutritionDal;
        public NutritionPersister(INutritionsDal nutritionDal)
        {
            this.nutritionDal = nutritionDal;
        }

        public Nutrition Insert(Nutrition nutrition)
        {

            Nutritions nutritions = new Nutritions();

            nutritions.carbohydrates = nutrition.carbohydrates;
            nutritions.protein = nutrition.protein;
            nutritions.sugar = nutrition.sugar;
            nutritions.fat = nutrition.fat;
            nutritions.calories = nutrition.calories;
            nutritions.fruitsid = nutrition.fruitsid;
            nutritionDal.Insert(nutritions);
            nutrition.id = nutritions.id;
            return nutrition;
        }
        public Nutrition Update(Nutrition nutrition)
        {
            Nutritions nutritions = new Nutritions();
            nutrition.id = nutritions.id;
            nutritions.carbohydrates = nutrition.carbohydrates;
            nutritions.protein = nutrition.protein;
            nutritions.sugar = nutrition.sugar;
            nutritions.fat = nutrition.fat;
            nutritions.calories = nutrition.calories;
            nutritions.fruitsid = nutrition.fruitsid;
            nutritionDal.Update(nutritions);

            return nutrition;
        }
        public Nutrition Delete(Nutrition nutrition)
        {
            Nutritions nutritions = new Nutritions();
            nutritions.carbohydrates = nutrition.carbohydrates;
            nutritions.protein = nutrition.protein;
            nutritions.sugar = nutrition.sugar;
            nutritions.fat = nutrition.fat;
            nutritions.calories = nutrition.calories;
            nutritions.fruitsid = nutrition.fruitsid;
            nutrition.id = nutritions.id;
            nutritionDal.Delete(nutritions);

            return nutrition;
        }

        public NutritionInfo GetFruits(int FruitId)
        {
            NutritionInfo nutrition = new NutritionInfo();
            var item = nutritionDal.FetchList(FruitId);
            if(item != null)
            {
                nutrition.fat = item.fat;
                nutrition.calories = item.calories;
                nutrition.carbohydrates = item.carbohydrates;
                nutrition.protein = item.protein;
                nutrition.sugar = item.sugar;
            }
            
            return nutrition;
        }
    }
}
