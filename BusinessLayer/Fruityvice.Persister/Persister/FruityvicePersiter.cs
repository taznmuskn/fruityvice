﻿using DBContext.Entity;
using Fruityvice.Dal;
using Fruityvice.Persister.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fruityvice.Persister.Persister
{
    public class FruityvicePersiter
    {
        private readonly FruitPersister fruitPersister;
        private readonly NutritionPersister nutritionPersister;

        public FruityvicePersiter(FruitPersister fruitPersister, NutritionPersister nutritionPersister)
        {
            this.fruitPersister = fruitPersister;
            this.nutritionPersister = nutritionPersister;   
        }

        public Fruityvices Insert(Fruityvices fruityvice)
        {
            Fruit fruits = new Fruit();
            fruits.genus = fruityvice.genus;
            fruits.family = fruityvice.family;
            fruits.name = fruityvice.name;
            fruits.order = fruityvice.order;
           var f= fruitPersister.Insert(fruits);
            Nutrition nutrition = new Nutrition();
            nutrition.fruitsid=f.id;
            nutrition.sugar=Convert.ToInt32(fruityvice.nutritionInfo.sugar);
            nutrition.protein = Convert.ToInt32(fruityvice.nutritionInfo.protein);
            nutrition.carbohydrates = Convert.ToInt32(fruityvice.nutritionInfo.carbohydrates);
            nutrition.calories = Convert.ToInt32(fruityvice.nutritionInfo.calories);
            nutrition.fat = Convert.ToInt32(fruityvice.nutritionInfo.fat);

            nutritionPersister.Insert(nutrition);
            return fruityvice;

        }
        public Fruityvices Update(Fruityvices fruityvice)
        {
            //fruitPersister.Update(fruityvice.fruitInfo);
            //nutritionPersister.Update(fruityvice.nutritionInfo);
            return fruityvice;
        }
        public Fruityvices Delete(Fruityvices fruityvice)
        {
            //fruitPersister.Delete(fruityvice.fruitInfo);
            //nutritionPersister.Delete(fruityvice.nutritionInfo);
            return fruityvice;
        }

        public List<Fruityvices> GetAllFruits()
        {
            List<Fruityvices> FruityvicesList = new List<Fruityvices>();
            var fruits = fruitPersister.GetFruits();
            foreach( var fruit in fruits )
            {
                Fruityvices fruityvices = new Fruityvices();
                fruityvices.id= fruit.id;
                fruityvices.genus= fruit.genus;
                fruityvices.name= fruit.name;
                fruityvices.family= fruit.family  ;
                fruityvices.order= fruit.order;
                fruityvices.nutritionInfo = nutritionPersister.GetFruits(fruit.id);
                FruityvicesList.Add(fruityvices);
            }


            return FruityvicesList;
        }
    }
}
