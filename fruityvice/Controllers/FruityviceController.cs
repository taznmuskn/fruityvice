﻿using Fruityvice.Persister.Model;
using Fruityvice.Persister.Persister;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace fruityvice.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FruityviceController : ControllerBase
    {
        private readonly FruityvicePersiter _persister;

        public FruityviceController(FruityvicePersiter persister)
        {
            _persister = persister;
        }

        [HttpGet]
        public IActionResult Get()
        {
            var fruits = _persister.GetAllFruits();

            return Ok(fruits);
        }
        [HttpPost]
        public IActionResult Post(Fruityvices fruityvices)
        {
            var fruityvice = _persister.Insert(fruityvices);
            return Ok(fruityvice);
        }
    }
}
