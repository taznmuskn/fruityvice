﻿using Fruityvice.Persister.Persister;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace fruityvice.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NutritionController : ControllerBase
    {
        private readonly NutritionPersister _persister;

        public NutritionController(NutritionPersister persister)
        {
            _persister = persister;
        }

        [HttpGet]
        public IActionResult Get(int Fruitid)
        {
            var fruits = _persister.GetFruits(Fruitid);

            return Ok(fruits);
        }

    }
}
