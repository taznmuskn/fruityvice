﻿using Fruityvice.Persister.Persister;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace fruityvice.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FruitController : ControllerBase
    {
        private readonly FruitPersister _persister;

        public FruitController(FruitPersister persister)
        {
            _persister = persister;
        }

        [HttpGet]
        public IActionResult Get()
        {
            var fruits=_persister.GetFruits();

            return Ok(fruits);
        }


    }
}
