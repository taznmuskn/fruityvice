using DBContext;
using Fruityvice.DalEF;
using Fruityvice.Persister.Model;
using Fruityvice.Persister.Persister;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddDbContext<FruityviceDBContext>(options =>
    options.UseNpgsql("Server=localhost;Port=5432;Database=FruityviceDB;User ID=myusername;Password=mypassword;"));

// Add services to the container.
builder.Services.AddDalEF();
builder.Services.AddModel();
builder.Services.AddPersister();
builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
