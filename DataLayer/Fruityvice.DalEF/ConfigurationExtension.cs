﻿using Fruityvice.Dal;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fruityvice.DalEF
{
    public static class ConfigurationExtension
    {
        public static void AddDalEF(this IServiceCollection services)
        {
            services.AddTransient<IFruitsDal, FruitsDaL>();
            services.AddTransient<INutritionsDal, NutritionsDal>();
        }
    }
}
