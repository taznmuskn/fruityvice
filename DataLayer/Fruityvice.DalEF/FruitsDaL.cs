﻿using DBContext;
using DBContext.Entity;
using Fruityvice.Dal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fruityvice.DalEF
{
    public class FruitsDaL : IFruitsDal
    {
        private readonly FruityviceDBContext dbContext;

        public FruitsDaL(FruityviceDBContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public void Delete(Fruits fruits)
        {
            var query = (from data in dbContext.Fruits where data.id == fruits.id  select data).First();

            dbContext.Attach(query);
            dbContext.Remove(query);
            dbContext.SaveChanges();
        }

        public List<Fruits> FetchList()
        {
           return dbContext.Fruits.ToList();
        }

        public void Insert(Fruits fruits)
        {
            dbContext.Fruits.Add(fruits);
            dbContext.SaveChanges();
        }

        public void Update(Fruits fruits)
        {
            var query = (from data in dbContext.Fruits where data.id == fruits.id select data).First();
            fruits = query;
            dbContext.Fruits.Add(fruits);
            dbContext.SaveChanges();
        }
    }
}
