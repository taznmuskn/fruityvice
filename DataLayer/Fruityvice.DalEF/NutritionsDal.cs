﻿using DBContext;
using DBContext.Entity;
using Fruityvice.Dal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fruityvice.DalEF
{
    public class NutritionsDal : INutritionsDal
    {
        private readonly FruityviceDBContext dbContext;

        public NutritionsDal(FruityviceDBContext dbContext)
        {
            this.dbContext = dbContext;
        }
        public void Delete(Nutritions nutritions)
        {
            var query = (from data in dbContext.Nutritions where data.id == nutritions.id  select data).First();

            dbContext.Attach(query);
            dbContext.Remove(query);
            dbContext.SaveChanges();
        }
        public Nutritions FetchList(int FruitId)
        {
            Nutritions nutritions= new Nutritions();
            nutritions= dbContext.Nutritions.Where(x => x.fruitsid == FruitId).FirstOrDefault();
            return nutritions;
        }
        public void Insert(Nutritions nutritions)
        {
            dbContext.Nutritions.Add(nutritions);
            dbContext.SaveChanges();
        }
        public void Update(Nutritions nutritions)
        {
            var query = (from data in dbContext.Nutritions where data.id == nutritions.id  select data).First();
            nutritions = query;
            dbContext.Nutritions.Add(nutritions);
            dbContext.SaveChanges();
        }
    }
}
