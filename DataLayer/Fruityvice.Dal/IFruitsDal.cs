﻿using DBContext.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fruityvice.Dal
{
    public interface IFruitsDal
    {
        List<Fruits> FetchList();
        void Insert(Fruits fruits);
        void Update(Fruits fruits);
        void Delete(Fruits fruits);
    }
}
