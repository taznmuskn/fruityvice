﻿using DBContext.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fruityvice.Dal
{
    public interface INutritionsDal
    {
        Nutritions FetchList(int FruitId);
        void Insert(Nutritions nutritions);
        void Update(Nutritions nutritions);
        void Delete(Nutritions nutritions);

    }
}
