﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DBContext.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace DBContext
{
    public class FruityviceDBContext : DbContext
    {
        public FruityviceDBContext() { }
        public FruityviceDBContext(DbContextOptions<FruityviceDBContext> options) : base(options) => Database.EnsureCreated();

        public virtual DbSet<Fruits> Fruits { get; set; }
        public virtual DbSet<Nutritions> Nutritions { get; set; }


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseNpgsql("Server=localhost;Port=5432;Database=FruityviceDB;User ID=myusername;Password=mypassword;");
            }
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasPostgresExtension("dblink");
            //modelBuilder.ApplyConfigurationsFromAssembly(typeof(Accesory).Assembly);
            base.OnModelCreating(modelBuilder);
        }
    }
}
