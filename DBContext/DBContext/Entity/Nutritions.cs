﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace DBContext.Entity
{
    [Table("Nutritions")]
    public class Nutritions
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("id")]
        public int id { get; set; }
        [Column("fruitsid")]
        [ForeignKey("Fruits")]
        public int fruitsid { get; set; }
        public virtual Fruits Fruits { get; set; }
        [Column("carbohydrates")]
        public int carbohydrates { get; set; }
        [Column("protein")]
        public int protein { get; set; }
        [Column("fat")]
        public int fat { get; set; }
        [Column("calories")]
        public int calories { get; set; }
        [Column("sugar")]
        public int sugar { get; set; }

    }
}
