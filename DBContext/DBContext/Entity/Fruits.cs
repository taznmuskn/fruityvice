﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace DBContext.Entity
{
    [Table("Fruits")]
    public class Fruits
    {
        [Column("name")]
        public string? name { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("id")]
        public int id { get; set; }
        [Column("family")]
        public string? family { get; set; }
        [Column("genus")]
        public string? genus { get; set; }
        [Column("order")]
        public string? order { get; set; }

    }
}
